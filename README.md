
# Teste ammo
## Instalação
Precisamos antes de rodar o projeto, preparar o MySQL com uma database e realizar a configurações começando pela API.

### API
- Instalar as dependências
	- `npm install`
- Configuração de DB nos arquivos:
	- `api/config/config.json` nó de `development`
	- `api/application/connection.js` 
- Alimentar DB com as seeds
	- `npm run seed:all`
- Iniciar a API Express
	-  `node index.js`
- Servidor estará na porta `:3001`

## WEB
- Instalar as dependências
	- `npm install`
- Executar o projeto OU Gerar build do projeto
	- `npm start` OU `npm run build`
- Projeto estará na porta `:3000`

