import axios from 'axios'

export const getProducts = async ({query, actualPage, limit}) => {
    let allProducts = await axios.post("http://localhost:3001/", { 
        page: actualPage,
        query,
        limit
    });
    return allProducts.data;
}