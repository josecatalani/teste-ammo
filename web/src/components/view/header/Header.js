import React, { Component } from 'react'
import Logo from '../../../assets/img/logo.svg'
import Search from '../../../assets/img/search.png'
import './Header.css'

class Header extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { query, onChange, onSubmit, onClearQuery } = this.props

        const hasQuery = query != "" ? true : false

        return (
            <div>
                <header id="main-header">
                    <div id="main-logo">
                        <img src={Logo} alt="MMartan" />
                    </div>
                    <div id="main-search">
                        <form onSubmit={onSubmit}>
                            <input type="text" placeholder="Buscar" onChange={onChange} value={query} />
                            { hasQuery ? (<button type="button" className="clear-query" onClick={onClearQuery}></button>) : null }
                        </form>
                    </div>
                </header>
                <div id="query-param">{query}</div>
            </div>
        )
    }
}

export default Header