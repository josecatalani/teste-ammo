import React, { Component } from 'react'
import Logo from '../../../assets/img/logo.svg'

import PlaceholderImg from '../../../assets/img/placeholder.jpeg'

class ProductItem extends Component {
    render() {
        const { id, title, categories, normalPrice, salesPrice, images, sizes } = this.props
        return (
            <div className="product-item">
                <div className="product-images">
                    {images.map((image, index) => <img key={index} src={image.src == "" ? PlaceholderImg : image.src}/>)}
                </div>
                <div className="product-description">
                    <h2>{title}</h2>
                    <p className="product-categories">
                        {categories.map((category, index, array) => {
                            let suffix = ' '
                            if(index < array.length - 1) {
                                suffix = ', '
                            }
                            return `${category.name}${suffix}`
                        })}
                        {' '}&bull;{' '}
                        {sizes.map((size, index, array) => {
                            let suffix = ' '
                            if(index < array.length - 1) {
                                suffix = ', '
                            }
                            return `${size.size}${suffix}`
                        })}
                    </p>
                </div>
                <div className="product-price">
                    <p>
                        <span className="normal-price">R$ {normalPrice}</span>
                        {' '} 
                        por
                        {' '}
                        <span className="sales-price"><b>R$ {salesPrice}</b></span>
                    </p>
                </div>
            </div>
        )
    }
}

export default ProductItem