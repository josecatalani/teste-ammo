import React, { Component } from 'react'
import ProductItem from './ProductItem'
import Footer from '../footer/Footer'
import './ProductsList.css'

class ProductsList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visibility: [16, 32, 48]
        }

        this.numberToReal = this.numberToReal.bind(this)
    }

    numberToReal(numero) {
        var numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
    }

    render() {
        const { products, actualPage, onLimitChange, limit, totalResults, totalPages, onClickPagination } = this.props

        var totalMessage = "";

        if (totalResults == 0) {
            totalMessage = "Nenhum produto encontrado"
        } else if (totalResults == 1) {
            totalMessage = `${totalResults} produto encontrado`
        } else {
            totalMessage = `${totalResults} produtos encontrados`
        }

        return (
            <div>
                <div className="container">
                    <div id="products-list" className="">
                        <header>
                            <div id="products-count">{totalMessage}</div>
                        </header>
                        <div>
                            <div id="products">
                                {products.length > 0 ? (products.map((product, index) => (
                                    <ProductItem
                                        key={index}
                                        id={product.id}
                                        title={product.title}
                                        categories={product.categories}
                                        images={product.images}
                                        sizes={product.sizes}
                                        normalPrice={this.numberToReal(product.normalPrice)}
                                        salesPrice={this.numberToReal(product.salesPrice)}
                                    />
                                ))) : (
                                        <p className="empty-results">Nenhum produto para visualizar</p>
                                    )}
                            </div>
                        </div>
                    </div>

                    <Footer
                        totalPages={totalPages}
                        limit={limit}
                        onClickPagination={onClickPagination}
                        onLimitChange={onLimitChange}
                        actualPage={actualPage}
                    />
                </div>
            </div>
        )
    }
}

export default ProductsList