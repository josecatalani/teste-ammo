import React, { Component } from 'react'
import Arrow from '../../../assets/img/arrow.svg'
import Pagination from './Pagination'
import './Footer.css'

class Footer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { totalPages, limit, onLimitChange, onClickPagination, actualPage } = this.props
        return (
            <div>
                <footer id="main-footer">
                    <div>
                        <select 
                            value={limit} 
                            onChange={onLimitChange}
                            style={{backgroundImage: "url(" + Arrow + ")"}}
                        >
                            <option value={8}>8 produtos por pagina</option>
                            <option value={16}>16 produtos por pagina</option>
                            <option value={32}>32 produtos por pagina</option>
                        </select>
                    </div>
                    <div>
                        <Pagination 
                            actualPage={actualPage}
                            totalPages={totalPages}
                            onClickPagination={onClickPagination}
                        />
                    </div>
                </footer>
            </div>
        )
    }
}

export default Footer