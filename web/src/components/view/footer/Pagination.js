import React, { Component } from 'react'

const PaginationItem = ({ number, onClickPagination, isActualPage }) => (
    <li className={isActualPage ? 'selected' : ''} onClick={() => {
        onClickPagination(number)
    }}>{number}</li>
)

class Pagination extends Component {
    constructor(props) {
        super(props)

        this.state = {
        }

        this.toFirstPage = this.toFirstPage.bind(this)
        this.toLastPage = this.toLastPage.bind(this)
        this.toPrevPage = this.toPrevPage.bind(this)
        this.toNextPage = this.toNextPage.bind(this)
    }

    toFirstPage() {
        this.props.onClickPagination(1)
    }

    toLastPage() {
        this.props.onClickPagination(this.props.totalPages)
    }

    toNextPage() {
        this.props.onClickPagination(this.props.actualPage + 1)
    }

    toPrevPage() {
        this.props.onClickPagination(this.props.actualPage - 1)
    }

    render() {
        const { totalPages, actualPage, limit } = this.props
        const { onClickPagination } = this.props

        if(totalPages <= 1) {
            return null
        }

        let items = [];

        const firstPage = actualPage == 1 ? true : false
        const lastPage = actualPage == totalPages ? true : false
        const paginationSize = limit

        var startPaging, endPaging

        if (totalPages <= 6) {
            startPaging = 1
            endPaging = totalPages
        } 
        else {
            if(actualPage <= 3) {
                startPaging = 1
                endPaging = 6
            } 
            else if (actualPage + 2 >= totalPages ){
                startPaging = totalPages - 5
                endPaging = totalPages
            } 
            else {
                startPaging = actualPage - 3
                endPaging = actualPage + 2
            }
        }

        console.log(totalPages, startPaging, endPaging)

        for (let i = startPaging; i < endPaging + 1; i++) {
            items.push(
                <PaginationItem
                    key={i}
                    number={i}
                    isActualPage={actualPage == (i)}
                    onClickPagination={onClickPagination}
                />
            )
        }

        return (
            <div id="pagination">
                <ul>
                    <li>
                        <button type="button" className={"paginate-btns first-page"} onClick={this.toFirstPage} disabled={firstPage}>&lsaquo;|</button>
                    </li>
                    <li>
                        <button type="button" className={"paginate-btns previous-page"} onClick={this.toPrevPage} disabled={firstPage}>&lsaquo;</button>
                    </li>
                    {items}
                    <li>
                        <button type="button" className={"paginate-btns next-page"} onClick={this.toNextPage} disabled={lastPage}>&rsaquo;</button>
                    </li>
                    <li>
                        <button type="button" className={"paginate-btns last-page"} onClick={this.toLastPage} disabled={lastPage}>&rsaquo;|</button>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Pagination