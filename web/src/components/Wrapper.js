import React, { Component } from 'react'
import Header from './view/header/Header'
import ProductsList from './view/product-list/ProductsList'
import { getProducts } from '../services/api.js'
import './Wrapper.css'

class Wrapper extends Component {
    constructor(props) {
        super(props)

        this.state = {
            totalResults: 0,
            query: '',
            totalPages: 1,
            actualPage: 1,
            limit: 8,
            products: []
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.onClearQuery = this.onClearQuery.bind(this)
        this.onLimitChange = this.onLimitChange.bind(this)
        this.onClickPagination = this.onClickPagination.bind(this)
    }

    componentDidMount() {
        this.getProductsList();
    }

    onChange(e) {
        this.setState({
            query: e.target.value
        })
    }

    onSubmit(e) {
        this.getProductsList();

        e.preventDefault();
    }

    getProductsList() {
        const { query, actualPage, limit } = this.state
        
        let params = {
            query: query,
            actualPage: actualPage,
            limit: limit
        }

        getProducts(params).then(data => {
            this.setState({
                products: data.products,
                totalPages: data.pages,
                totalResults: data.total
            })
        });
    }

    onClearQuery() {
        this.setState({
            query: ''
        })
    }

    onLimitChange(e) {
        this.setState({
            actualPage: 1,
            limit: parseInt(e.target.value)
        }, () => {
            this.getProductsList()
        })
    }

    onClickPagination(value) {
        this.setState({
            actualPage: value
        }, () => {
            this.getProductsList()
        })
    }

    render() {
        const { query, products, actualPage, limit, totalResults, totalPages } = this.state

        return (
            <div>
                <Header
                    onSubmit={this.onSubmit}
                    onChange={this.onChange}
                    onClearQuery={this.onClearQuery}
                    query={query}
                />
                <ProductsList
                    onLimitChange={this.onLimitChange}
                    limit={limit}
                    onClickPagination={this.onClickPagination}
                    totalPages={totalPages}
                    totalResults={totalResults}
                    actualPage={actualPage}
                    products={products}
                />
            </div>
        )
    }
}

export default Wrapper