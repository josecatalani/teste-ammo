'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    var insertData = []
    for(var i = 0; i < 100; i ++) {
      let randomPrice = Math.floor(Math.random() * 150) + 1

      insertData.push({
        title: `${i} Produto`,
        normalPrice: randomPrice,
        salesPrice: parseFloat(randomPrice * 0.8) 
      })
    }
    return queryInterface.bulkInsert('Products', insertData, {});
  },
  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('Products', null, {});
  }
};
