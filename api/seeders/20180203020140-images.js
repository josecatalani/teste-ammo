'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    var insertData = []
    for(var i = 1; i <= 100; i ++) {
      insertData.push({
        productId: i,
        src: 'https://dgn7v532p0g5j.cloudfront.net/unsafe/144x144/products/photos/semi-environment/LSNWG.PG1P18RCa.png.1503599959538.jpeg' 
      },{
        productId: i,
        src: 'https://dgn7v532p0g5j.cloudfront.net/unsafe/144x144/products/photos/semi-environment/LSNWG.PG1P18RCa.png.1503599959538.jpeg' 
      },{
        productId: i,
        src: 'https://dgn7v532p0g5j.cloudfront.net/unsafe/144x144/products/photos/semi-environment/LSNWG.PG1P18RCa.png.1503599959538.jpeg' 
      },{
        productId: i,
        src: 'https://dgn7v532p0g5j.cloudfront.net/unsafe/144x144/products/photos/semi-environment/LSNWG.PG1P18RCa.png.1503599959538.jpeg' 
      })
    }
    return queryInterface.bulkInsert('Images', insertData, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('Images', null, {});
  }
};
