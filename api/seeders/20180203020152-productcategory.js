'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    var insertData = []
    for(var i = 1; i <= 100; i ++) {
      let randomCategory = Math.floor(Math.random() * 10) + 1

      insertData.push({
        productsId: i,
        categoryId: randomCategory,
      })
    }
    return queryInterface.bulkInsert('Product_Categories', insertData, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('Product_Categories', null, {});
  }
};
