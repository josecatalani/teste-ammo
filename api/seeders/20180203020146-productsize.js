'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    var insertData = []
    for(var i = 1; i <= 100; i ++) {
      insertData.push({
        productsId: i,
        sizeId: Math.floor(Math.random() * 6) + 1 
      })
    }
    return queryInterface.bulkInsert('Product_Sizes', insertData, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkInsert('Product_Sizes', null, {});
  }
};
