const Sequelize = require('sequelize')
const async = require('async')

const sequelize = new Sequelize('teste_ammo', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        timestamps: false
    }
})

module.exports = sequelize