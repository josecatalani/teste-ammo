const Sequelize = require('sequelize');
const sequelize = require('./connection');
const async = require('async')

const Products = require('./models/Products')
const Images = require('./models/Images')
const Categories = require('./models/Categories')
const Sizes = require('./models/Sizes')
const ProductSize = require('./models/ProductSize')
const ProductCategory = require('./models/ProductCategory')

Products.belongsToMany(Categories, { through: { model: ProductCategory }, foreignKey: 'productsId' })
Products.belongsToMany(Sizes, { through: { model: ProductSize }, foreignKey: 'productsId' })
Products.hasMany(Images, { as: 'images' })

sequelize.sync({
    force: true
})
    .then(() => {
    })