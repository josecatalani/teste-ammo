const Products = require('../models/Products')

module.exports.index = (req, res) => {
    let params = req.body
    let page = params.page
    let limit = params.limit

    let whereConds = {}

    if(params.query != "") {
        whereConds.title = {
            $like: `%${params.query}%`
        }
    }

    Products.findAndCountAll({
        where: whereConds
    }).then(data => {
        let pages = Math.ceil(data.count / limit)
        let offset = limit * (page - 1);

        Products.findAll({
            include: [{ all: true }],
            where: whereConds,
            limit: limit,
            order: [
                ['title', 'ASC']
            ],
            offset: offset,
        })
        .then(products => {
            res.json({
                'products': products, 
                'total': data.count, 
                'pages': pages 
            })
        })
    })
}