const Sequelize = require('sequelize');
const sequelize = require('../connection');

const Products = sequelize.define('products', {
    title: {
        type: Sequelize.STRING
    },
    normalPrice: {
        type: Sequelize.FLOAT
    },
    salesPrice: {
        type: Sequelize.FLOAT
    }
});

module.exports = Products