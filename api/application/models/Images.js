const Sequelize = require('sequelize');
const sequelize = require('../connection');

const Images = sequelize.define('images', {
    src: {
        type: Sequelize.STRING
    }
});

module.exports = Images