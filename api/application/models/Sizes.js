const Sequelize = require('sequelize');
const sequelize = require('../connection');

const Sizes = sequelize.define('sizes', {
    size: {
        type: Sequelize.STRING
    }
});

module.exports = Sizes