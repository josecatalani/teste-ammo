const Sequelize = require('sequelize');
const sequelize = require('../connection');

const Categories = sequelize.define('categories', {
    name: {
        type: Sequelize.STRING
    }
});

module.exports = Categories