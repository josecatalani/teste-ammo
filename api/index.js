const express = require('express')
const async = require('async')
const Sequelize = require('sequelize');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser')
const cors = require('cors')

const sequelize = require('./application/connection')
const models = require('./application/models')
const routes = require('./application/routes')

const corsOptions = {
    origin: true,
    methods: ['POST'],
    credentials: true,
    enablePreflight: true,
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors(corsOptions))

app.options('/', routes.index)
app.post('/', routes.index)

app.listen(3001, () => console.log('API Up port:3001'))